﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace KinectModule
{
    class KinectModule
    {
        static Socket soc;
        static TcpListener listener;
        static Stream s;
        static StreamReader sr;
        static StreamWriter sw;
        static KinectST kinect;



        /// <summary>
        /// Starts the kinect sensor and tilts its angle to 0º
        /// </summary>
        private static void Start_Kinect()
        {
            System.Console.WriteLine("Start Kinect");
            //start kinect sensor
            System.Console.WriteLine("Connecting kinect ...");// posso enviar informação da conecção da kinect
            while (!kinect.isKinectReady())
            {
                kinect.StartKinectSTWithSmoothing();
            }
            kinect.tilt(0);
            System.Console.WriteLine("Kinect available ...");
        }
        /*
                private static void send_Coords()
                {
                    Point player = new Point(kinect.get_Player());
                    Point head = new Point(kinect.get_Head());
                    try
                    {
                        sw.WriteLine("{0} {1} {2} {3} {4} {5}",
                            player.get_X().ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                            player.get_Y().ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                            player.get_Z().ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                            head.get_X().ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                            head.get_Y().ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                            head.get_Z().ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error!!"+e.ToString());
                        return;
                    }
                }
                */

        /// <summary>
        /// Send the position of the player and its head
        /// </summary>
        public static void send_Coords(float[] player, float[] head)
        {
            try
            {
                sw.WriteLine("{0} {1} {2} {3} {4} {5}",
                    player[0].ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    player[1].ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    player[2].ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    head[0].ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    head[1].ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    head[2].ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                Console.WriteLine("sent");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error!!" + e.ToString());
                return;
            }
        }

        /// <summary>
        /// Stops the kthe sensor and exits the program
        /// </summary>
        private static void Stop_Kinect()
        {
            Console.WriteLine("Stop Kinect.");
            kinect.sensor.Stop();
            Environment.Exit(0);           
        }

        /// <summary>
        /// Opens de comunication chanels and defines the services provided
        /// </summary>
        private static void Service()
        {
            soc = listener.AcceptSocket();
            s = new NetworkStream(soc);
            sr = new StreamReader(s);
            sw = new StreamWriter(s);
            sw.AutoFlush = true;
            while (true)
            {
                string r = sr.ReadLine();
                int query = int.Parse(r);
                switch(query)
                {
                    case 0://Close the program
                        Console.WriteLine("Exit.");
                        Stop_Kinect();                        
                        break;
                    case 1://ask for new coordinates
                        Start_Kinect();
                        break;                    
                    default://error: unrecognized service
                        Console.WriteLine("Query not implemented : {0} ",query);
                        break;
                }
            }
        }

        static void Main(string[] args)
        {
            kinect = new KinectST();

            //thread to listen unity requests
            listener = new TcpListener(2055);
            listener.Start();
            Thread t = new Thread(new ThreadStart(Service));
            t.Start();

            //loop to maintain the program running
            while (true)
            {

            }

        }
    }
}

