﻿using System;
using System.Linq;
using Microsoft.Kinect;
using System.IO;

namespace KinectModule
{
    class KinectST
    {
        /// <summary>
        /// Kinect sensor Instance
        /// </summary>
        public KinectSensor sensor = null;

        /// <summary>
        /// Skeleton Data
        /// </summary>
        private Skeleton[] skeletonData = null;


        //pontos que vão ser passados ao unity
        private float[] player;
        private float[] head;


        //diz se a kinect foi inicializada
        private bool instance = false;

        

        /// <summary>
        /// Start Kinect sensor a Skeleton traking
        /// </summary> 

        // Some smoothing with little latency (defaults).
        // Only filters out small jitters.
        // Good for gesture recognition in games. 

        /// <summary>
        /// Retrieves the Skeleton Data at each frame with smoothing
        /// </summary>
        public void StartKinectSTWithSmoothing()
        {
            sensor = KinectSensor.KinectSensors.FirstOrDefault(s => s.Status == KinectStatus.Connected); // Get first Kinect Sensor
            //sensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated; // Use Seated Mode

            TransformSmoothParameters smoothingParam = new TransformSmoothParameters();
            {
              smoothingParam.Smoothing = 0.5f;
              smoothingParam.Correction = 0.5f;
              smoothingParam.Prediction = 0.2f;
              smoothingParam.JitterRadius = 0.05f;
              smoothingParam.MaxDeviationRadius = 0.05f;
            };

            sensor.SkeletonStream.Enable(smoothingParam); // Enable skeletal tracking

            // Allocate ST data
            this.skeletonData = new Skeleton[sensor.SkeletonStream.FrameSkeletonArrayLength];

            sensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(kinect_SkeletonFrameReady); // Get Ready for Skeleton Ready Events
            initPos();
            sensor.Start(); // Start Kinect sensor
            
        }

    

        /// <summary>
        /// Retrieves the Skeleton Data at each frame
        /// </summary>
        private void kinect_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame()) // Open the Skeleton frame
            {
                if (skeletonFrame != null && this.skeletonData != null) // check that a frame is available
                {
                    skeletonFrame.CopySkeletonDataTo(this.skeletonData); // get the skeletal information in this frame
                }
                this.act_Positions();
            }
        }

        /// <summary>
        /// Start Kinect sensor a Skeleton traking without any smoothing
        /// </summary>
        public void StartKinectST()
        {
            sensor = KinectSensor.KinectSensors.FirstOrDefault(s => s.Status == KinectStatus.Connected); // Get first Kinect Sensor

            if (null != this.sensor)
            {
                // Turn on the skeleton stream to receive skeleton frames
                this.sensor.SkeletonStream.Enable();

                // Allocate ST data
                this.skeletonData = new Skeleton[sensor.SkeletonStream.FrameSkeletonArrayLength];

                this.sensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(kinect_SkeletonFrameReady); // Get Ready for Skeleton Ready Events

                // Start the sensor!
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }
            initPos();
        }

        /// <summary>
        /// Tilt the kinect sensor to the given angle accordingly with the earth gravity
        /// </summary>
        public void tilt(int angle)
        {
            sensor.ElevationAngle = angle;
        }

        /// <summary>
        /// Init the player position at (0,0,0)
        /// </summary>
        private void initPos()
        {
            player = new float[3] { 0.0f, 0.0f, 0.0f };
            head = new float[3] { 0.0f, 0.0f, 0.0f };
        }

        /// <summary>
        /// Returns the state of the sensor
        /// </summary>
        public bool isKinectReady()
        {
            return this.sensor != null;
        }

        /* /// <summary>
         /// Prints the position of the head, central shoulder and skeleton
         /// </summary>
         private void print_Positions()
         {
             foreach (Skeleton skeleton in this.skeletonData)
             {
                 Console.WriteLine("Head position -> x = {0}, y = {1}, z = {2};", skeleton.Joints[JointType.Head].Position.X, skeleton.Joints[JointType.Head].Position.Y, skeleton.Joints[JointType.Head].Position.Z);
                 Console.WriteLine("Central shoulder position -> x = {0}, y = {1}, z = {2};", skeleton.Joints[JointType.ShoulderCenter].Position.X, skeleton.Joints[JointType.ShoulderCenter].Position.Y, skeleton.Joints[JointType.ShoulderCenter].Position.Z);
                 Console.WriteLine("Skeleton position -> x = {0}, y = {1}, z = {2};", skeleton.Position.X, skeleton.Position.Y, skeleton.Position.Z);
             }
         }*/

        /// <summary>
        /// Update the player position e send it
        /// </summary>
        private void act_Positions()
        {
            //starts by checking the closest skeleton (player)
            Skeleton closestSkeleton = new Skeleton();
            float closestDistance = 1000f;
            foreach (Skeleton skeleton in this.skeletonData)
            {
                //update the positions
                if (skeleton.Position.Z < closestDistance && skeleton.Position.Z != 0)
                {
                    closestSkeleton = skeleton;
                    closestDistance = skeleton.Position.Z;
                }       
            }
            //save the new positions
            lock (player)
            {
                float[] p = new float[3] { closestSkeleton.Position.X, closestSkeleton.Position.Y, closestSkeleton.Position.Z };
                if (!(p[0] == 0.0f || p[1] == 0.0f || p[2] == 0.0f))
                {
                    player = p;
                }

            }
            lock (head)
            {
                float[] h = new float[3] { closestSkeleton.Joints[JointType.Head].Position.X, closestSkeleton.Joints[JointType.Head].Position.Y, closestSkeleton.Joints[JointType.Head].Position.Z };
                if (!(h[0] == 0.0f || h[1] == 0.0f || h[2] == 0.0f))
                {
                    head = h;
                }
            }
            //send the new positions
            lock (player){
                lock (head){
                    KinectModule.send_Coords(player, head);
                }
            }
        }

        /*public static void sendTest()
        {
            Point h = new Point(1, 2, 3);
            Point p = new Point(3, 2, 1);
            while(true)
            {
                KinectModule.send_Coords(p, h);
            }
        }*/
        /// <summary>
        /// returns the player position
        /// </summary>
        public float[] get_Player()
        {
            float[] r = null;
            lock(player)
            {
                r = new float[3] { player[0], player[1], player[2] };
            }
            return r;
        }

        /// <summary>
        /// returns the head position
        /// </summary>
        public float[] get_Head()
        {

            float[] r = null;
            lock (head)
            {
                r = new float[3] { head[0], head[1], head[2] };
            }
            return r;
        }
    }
}


// ainda não tenho método para desligar a kinect
//falta comentar melhor para decumentação