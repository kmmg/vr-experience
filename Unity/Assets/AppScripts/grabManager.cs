﻿using UnityEngine;
using System.Collections;

public class grabManager : MonoBehaviour {

	public AudioSource punch;
	bool colliding;
	public bool grabbed;
	public bool grabbing;

	public Material red;
	public Material blue;

	float startTime;
	float roundTimeSeconds;
	float roundTimeLeft;

	// Use this for initialization
	void Start ()
	{
		grabbed = false;
		colliding = false;
		roundTimeSeconds = 1.5f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (colliding && !grabbed) 
		{
			roundTimeLeft = Time.time - startTime;
			if (roundTimeLeft >= roundTimeSeconds) 
			{
				Debug.Log("grab");
				this.renderer.material = blue;
				grabbed = true;
			}
		}
	}

	IEnumerator restartAudio()
	{
		yield return new WaitForSeconds(0.25f);
		//playedOnce = false;
	}

	void OnCollisionEnter(Collision collision) 
	{	
		if (!colliding)
		{
			startTime = Time.time;
			this.renderer.material = red;

		}
		Debug.Log("colliding");
		colliding = true;

		//Debug.Log("colliding with "+collision.collider);
	}
	void OnCollisionExit(Collision collision)
	{
		colliding = false;
		if (!grabbed) 
		{
			this.renderer.material = blue;
		}

		//grabbed = false;

		Debug.Log("not colliding");
	}
}