﻿using UnityEngine;
using System.Collections;
using Leap;

public class toolManager : MonoBehaviour 
{
	public GameObject toolObject;
	
	public AudioSource audioFeedBack;

	Controller acessToLeap;
	Frame currentFrame;

	grabManager toolObjectManager;

	void Awake() 
	{
		acessToLeap = new Controller();
	}

	// Use this for initialization
	void Start () 
	{
		toolObjectManager = toolObject.GetComponent<grabManager>();


		if (acessToLeap != null) 
		{
			//acessToLeap.EnableGesture (Gesture.GestureType.TYPE_SCREEN_TAP);
			acessToLeap.EnableGesture (Gesture.GestureType.TYPESWIPE);
		} 
		else 
		{
			Debug.Log("error setting gestures");
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if (acessToLeap == null)
		{
			return;
		}
		else
		{
			acessToLeap.SetPolicyFlags(Controller.PolicyFlag.POLICY_IMAGES | Controller.PolicyFlag.POLICY_OPTIMIZE_HMD);		
		}

		currentFrame = acessToLeap.Frame();

		HandList hands = currentFrame.Hands;

		if (toolObjectManager.grabbed) 
		{
			if (hands.Count == 1) 
			{
				Debug.Log("teste");
				Vector3 newPosition = hands[0].PalmPosition.ToUnityScaled(false);

				toolObject.transform.position = this.transform.TransformPoint(newPosition);
				toolObject.renderer.material = toolObjectManager.blue;
			}
		}


		GestureList detectedGestures = currentFrame.Gestures();
		processGestures(detectedGestures);
	}

	void processGestures(GestureList userGestures)
	{
		if (!userGestures.IsEmpty)
		{
			string gestureType = userGestures[0].ToString();
			HandList handGesture = userGestures[0].Hands;

			if(handGesture[0].IsLeft)
			{
				toolObjectManager.grabbed = false;
				Debug.Log(gestureType + " on the left");
				audioFeedBack.Play();
			}
			else
			{
				toolObjectManager.grabbed = false;
				Debug.Log(gestureType + " on the right");
				audioFeedBack.Play();
			}		
		}
	}
}

/*
		if (hands.Count == 0)
		{
			leftHandObject.renderer.enabled = false;
			leftHandObject.transform.position = new Vector3(0,0,0);
			rightHandObject.renderer.enabled = false;
			rightHandObject.transform.position = new Vector3(0,0,0);
		}
		else if (hands.Count == 1) 
		{

			if(hands[0].Confidence > 0)
			{
				Vector3 newPosition = hands[0].Arm.WristPosition.ToUnityScaled(false);
				Quaternion newRotation = this.transform.rotation * hands[0].Basis.Rotation(false);

				if(hands[0].IsLeft)
				{
					//update and show left hand
					leftHandObject.transform.position = this.transform.TransformPoint(newPosition);
					leftHandObject.transform.rotation = newRotation;
					leftHandObject.renderer.enabled = true;
					//hide right hand
					rightHandObject.renderer.enabled = false;
					rightHandObject.transform.position = new Vector3(0,0,0);
				}
				else
				{
					//update and show right hand
					rightHandObject.transform.position = this.transform.TransformPoint(newPosition);
					rightHandObject.transform.rotation = newRotation;
					rightHandObject.renderer.enabled = true;
					//hide left hand
					leftHandObject.renderer.enabled = false;
					leftHandObject.transform.position = new Vector3(0,0,0);
				}
			}
		}
		else if(hands.Count == 2)
		{
			leftHandObject.renderer.enabled = true;
			rightHandObject.renderer.enabled = true;

			Vector3 newPosition = hands[0].Arm.WristPosition.ToUnityScaled(false);
			Quaternion newRotation = this.transform.rotation * hands[0].Basis.Rotation(false);
			
			if(hands[0].IsLeft)
			{
				leftHandObject.transform.position = this.transform.TransformPoint(newPosition);
				leftHandObject.transform.rotation = newRotation;
				leftHandObject.SetActive(true);
			}
			else
			{
				rightHandObject.transform.position = this.transform.TransformPoint(newPosition);
				rightHandObject.transform.rotation = newRotation;
				rightHandObject.SetActive(true);
			}

			Vector3 newPosition2 = hands[1].Arm.WristPosition.ToUnityScaled(false);
			Quaternion newRotation2 = this.transform.rotation * hands[1].Basis.Rotation(false);

			if(hands[1].IsLeft)
			{
				leftHandObject.transform.position = this.transform.TransformPoint(newPosition2);
				leftHandObject.transform.rotation = newRotation2;
				leftHandObject.SetActive(true);

			}
			else
			{
				rightHandObject.transform.position = this.transform.TransformPoint(newPosition2);
				rightHandObject.transform.rotation = newRotation2;
				rightHandObject.SetActive(true);
			}
		}*/