﻿using UnityEngine;
using System.Collections;

public class collisonManager : MonoBehaviour {

	public AudioSource punch;
	bool playedOnce;

	// Use this for initialization
	void Start ()
	{
		playedOnce = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator restartAudio()
	{
		yield return new WaitForSeconds(0.25f);
		playedOnce = false;
	}

	void OnCollisionEnter(Collision collision) 
	{
		if (!playedOnce)
		{
			punch.Play();
			playedOnce = true;
		}

	}
	void OnCollisionExit(Collision collision)
	{
		//playedOnce = false;
		StartCoroutine (restartAudio ());
	}
}

//Debug.Log("para ja chega");
//punch.Play();

/*foreach (ContactPoint contact in collision.contacts) {
			Debug.DrawRay(contact.point, contact.normal, Color.white);
		}
		if (collision.relativeVelocity.magnitude > 2)
			audio.Play();*/