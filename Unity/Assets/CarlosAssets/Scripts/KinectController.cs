using UnityEngine;
using System.Collections;

public class KinectController : MonoBehaviour 
{
	/// <summary>
	/// The head.
	/// </summary>
	public GameObject head;
	/// <summary>
	/// The speed.
	/// </summary>
	public float speed = 10.0f;
	/// <summary>
	/// The whitespace.
	/// </summary>
	private char[] whitespace = new char[] { ' ', '\t' };
	/// <summary>
	/// The socket.
	/// </summary>
	private s_TCP socket = new s_TCP();
	/// <summary>
	/// The scale.
	/// </summary>
	public float scale = 10;
/// <summary>
/// The wallback.
/// </summary>
	public GameObject wallback;
	/// <summary>
	/// The wallfront.
	/// </summary>
	public GameObject wallfront;
	/// <summary>
	/// The wallright.
	/// </summary>
	public GameObject wallright;
	/// <summary>
	/// The wallleft.
	/// </summary>
	public GameObject wallleft;


	/// <summary>
	/// The max x position.
	/// </summary>
	private float max_X;
	/// <summary>
	/// The max z.
	/// </summary>
	private float max_Z;
	/// <summary>
	/// The min_ x position.
	/// </summary>
	private float min_X;
	/// <summary>
	/// The min z position.
	/// </summary>
	private float min_Z;


	/// <summary>
	/// The c.
	/// Var to control if the warning image apears or not
	/// </summary>
	private int c ;


	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () 
	{
		socket.Start ();
		Transform t = GameObject.Find ("WallRight").transform;
		max_X = t.position.x - t.localScale.x;
		t = GameObject.Find ("Table").transform;
		min_X = t.position.x + (t.localScale.x /2f);
		t = GameObject.Find ("WallFront").transform;
		max_Z = t.position.z - t.localScale.x;
		t = GameObject.Find ("WallBack").transform;
		min_Z = t.position.z + t.localScale.x;


	}

	

	/// <summary>
	/// Fixeds the update.
	/// Moves the head to the new position inside the limits. Blocks the coordinate (x or z) if it is on its limit.
	/// If player is in the limits changes de color of the walls
	/// </summary>
	void FixedUpdate () 
	{
		bool limit = false;
		//new head position
		Vector3 nhp = socket.getHeadPosition () * scale;
		//limit player movements inside de scene
		//back
		if (nhp.z < min_Z) {
			nhp.z = min_Z;
			limit = true;
		//guiEnable = true;
		}
				//front
		if (nhp.z > max_Z) {
			nhp.z = max_Z;
			limit = true;
		//guiEnable = true;
		}
						//left
		if (nhp.x < min_X) {
			nhp.x = min_X;
			//limit = true;
		//guiEnable = true;
		} 
		//right
		if (nhp.x > max_X) {
			nhp.x = max_X;
			limit = true;
		//guiEnable = true;
		}

		if (!limit) 
		{
			wallback.renderer.material.color = new Color(0.898f, 0.949f, 0.945f);
			wallfront.renderer.material.color = new Color(0.898f, 0.949f, 0.945f);
			wallleft.renderer.material.color = new Color(0.898f, 0.949f, 0.945f);
			wallright.renderer.material.color = new Color(0.898f, 0.949f, 0.945f);
		} else 
		{
			wallback.renderer.material.color = new Color(0.9f, 0.08f, 0.08f);
			wallfront.renderer.material.color = new Color(0.9f, 0.08f, 0.08f);
			wallleft.renderer.material.color = new Color(0.9f, 0.08f, 0.08f);
			wallright.renderer.material.color = new Color(0.9f, 0.08f, 0.08f);
		}

		//bool active = c > canvasThreshold;
	
		//canvas1.SetActive (active);
		//canvas2.SetActive (active);
		
		head.transform.position = Vector3.Lerp(head.transform.position,nhp ,speed * Time.deltaTime);
		//Debug.Log ("Head position -> x = " + nhp.x + ", y = " + nhp.y + ", z = " + nhp.z + ";");

		}

	/// <summary>
	/// Raises the application quit event.
	/// Send the service t close the kinect module.
	/// </summary>
		void OnApplicationQuit()
		{
				socket.quit();

		}
}
