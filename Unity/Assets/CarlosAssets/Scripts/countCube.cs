﻿using UnityEngine;
using System.Collections;

public class countCube : MonoBehaviour {
	/// <summary>
	/// The n cubes.
	/// </summary>
	private int nCubes;
	/// <summary>
	/// The number of cubes.
	/// </summary>
	public int Cubes = 2;

	/// <summary>
	/// The UI actual number of cubes found.
	/// </summary>
	public GameObject UI_nCubes;
	/// <summary>
	/// The UI instruction.
	/// </summary>
	public GameObject UI_Instruction;
	/// <summary>
	/// The UI total number of cubes needed.
	/// </summary>
	public GameObject UI_Cubes;
	/// <summary>
	/// The UI result.
	/// </summary>
	public Sprite UI_Result;
	/// <summary>
	/// The numbers.
	/// </summary>
	public Sprite[] numbers;


	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
		this.nCubes = 0;
		SpriteRenderer ncubes = UI_nCubes.GetComponent<SpriteRenderer>();
		ncubes.sprite = numbers [0];
		SpriteRenderer total = UI_Cubes.GetComponent<SpriteRenderer>();
		total.sprite = numbers [Cubes];
	}

/// <summary>
/// Raises the trigger enter event.
	/// Update the cube counter UI if the cube that enter the box is the right one.
/// </summary>
	/// <param name="collider">collider.</param>
	void OnTriggerEnter(Collider collider)
	{
		if(collider.gameObject.tag == "count")
		{
			nCubes++;
			/*
			UI_nCubes.GetComponent<TextMesh>().text = nCubes+"/"+Cubes;
			if(nCubes==Cubes)
			{
				UI_nCubes.GetComponent<TextMesh>().text = "";
				UI_Instruction.GetComponent<TextMesh>().text = "";
				UI_Cubes.GetComponent<TextMesh>().text = "";
				UI_Result.GetComponent<TextMesh>().text = "You won!!!!";
			}*/
			collider.gameObject.tag = "ncount";
			//Destroy(col.gameObject);
		}	
		UpdateUI ();
	}
	/// <summary>
	/// Updates the UI.
	/// </summary>
	void UpdateUI()
	{
		if (nCubes == Cubes) {
						Destroy (UI_Cubes);
						Destroy (UI_nCubes);
						SpriteRenderer result = UI_Instruction.GetComponent<SpriteRenderer> ();
						result.sprite = UI_Result;
		} else 
		{
			SpriteRenderer ncubes = UI_nCubes.GetComponent<SpriteRenderer>();
			ncubes.sprite = numbers [nCubes];
		}

	}
}
