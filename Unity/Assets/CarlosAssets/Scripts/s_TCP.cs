﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;
//controls teh comunication with the kinect module

public class s_TCP 
{/// <summary>
/// The socket ready.
/// </summary>
    internal Boolean socketReady = false;
	/// <summary>
	/// The m_ thread.
	/// </summary>
	private System.Threading.Thread m_Thread = null;
	/// <summary>
	/// The whitespace.
	/// </summary>
	private char[] whitespace = new char[] { ' ', '\t' };
	/// <summary>
	/// The head.
	/// </summary>
	protected Vector3 head;
	/// <summary>
	/// The body.
	/// </summary>
	protected Vector3 body;
	/// <summary>
	/// The running.
	/// </summary>
    private bool running = true;
	/// <summary>
	/// My socket.
	/// </summary>
    TcpClient mySocket;
	/// <summary>
	/// The stream.
	/// </summary>
    NetworkStream theStream;
	/// <summary>
	/// The writer.
	/// </summary>
    StreamWriter theWriter;
	/// <summary>
	/// The reader.
	/// </summary>
    StreamReader theReader;
	/// <summary>
	/// The host.
	/// </summary>
    String Host = "192.168.1.2";
	/// <summary>
	/// The port.
	/// </summary>
    Int32 Port = 2055;


    // **********************************************
	//creat a new thread for the comunications
	/// <summary>
	/// Start this instance.
	/// Create a new thread for the comunications.
	/// </summary>
	public virtual void Start()
	{
		m_Thread = new System.Threading.Thread(Run);
		m_Thread.Start();
	}
	/// <summary>
	/// Gets the head position.
	/// </summary>
	/// <returns>The head position.</returns>
	public virtual Vector3 getHeadPosition(){
		Vector3 r = head;
			return r;
		}

	/// <summary>
	/// Gets the body position.
	/// </summary>
	/// <returns>The body position.</returns>
	public virtual Vector3 getBodyPosition(){
		Vector3 r = body;
			return r;
	}

	/// <summary>
	/// Stop this instance.
	/// </summary>
	public virtual void stop(){
        this.running = false;
		writeSocket ("0");
		}

	/// <summary>
	/// Setups the socket.
	/// </summary>
    private void setupSocket()
    {
        try
        {
            mySocket = new TcpClient(Host, Port);
            theStream = mySocket.GetStream();
            theWriter = new StreamWriter(theStream);
            theReader = new StreamReader(theStream);
			theReader.BaseStream.ReadTimeout = 5;
            socketReady = true;
        }
        catch (Exception e)
        {
            Debug.Log("Socket error: " + e);
        }
    }
	/// <summary>
	/// Quit this instance.
	/// </summary>
	public void quit(){
		writeSocket ("0");
		m_Thread.Abort ();
	}

	/// <summary>
	/// Writes the socket.
	/// </summary>
	/// <param name="theLine">The line.</param>
    public void writeSocket(string theLine)
    {
             if (!socketReady)
            return;
        String foo = theLine + "\r\n";
        theWriter.Write(foo);
        theWriter.Flush();
         }


	/// <summary>
	/// Reads the socket.
	/// </summary>
	/// <returns>The socket.</returns>
    private String readSocket()
    {
        if (!socketReady)
            return "";
        try
        {
            return theReader.ReadLine();
        }
        catch (Exception e)
        {
            return "time out";
        }
    }

	/// <summary>
	/// Closes the socket.
	/// </summary>
    private void closeSocket()
    {
        if (!socketReady)
                     return;
        theWriter.Close();
        theReader.Close();
        mySocket.Close();
        socketReady = false;
         }

	/// <summary>
	/// Threads the function.
	/// </summary>
	protected virtual void ThreadFunction() {
        while (running)
        {
            string r = readSocket();
            //if could not read from the socket
            if (r.Equals("time out"))
            {
                Debug.Log("time out");
                continue;
            }
            string[] coords = r.Split(whitespace);
            if (coords.Length == 6)
            {
                Vector3 skeleton = new Vector3(
                    float.Parse(coords[0], System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(coords[1], System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    -float.Parse(coords[2], System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                Vector3 shead = new Vector3(
                    float.Parse(coords[3], System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    float.Parse(coords[4], System.Globalization.CultureInfo.InvariantCulture.NumberFormat),
                    -float.Parse(coords[5], System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                //Debug.Log ("Head position -> x = " + shead.x + ", y = " + shead.y + ", z = " + shead.z + ";");
                //Debug.Log ("Skeleton position -> x = " + skeleton.x + ", y = " + skeleton.y + ", z = " + skeleton.z + ";\n");
				//falta limitar o movimento
                head = shead;
                body = skeleton;

            }
            else {
                Debug.Log("Message not recognized: " + "'" + r + "'");
				this.m_Thread.Abort();
            }
        }
	}

	/// <summary>
	/// Run this instance.
	/// </summary>
	private void Run()
	{
		setupSocket ();
        writeSocket("1");
		ThreadFunction();
	}
} // end class s_TCP
