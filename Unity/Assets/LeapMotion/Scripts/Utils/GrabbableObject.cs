﻿/******************************************************************************\
* Copyright (C) Leap Motion, Inc. 2011-2014.                                   *
* Leap Motion proprietary. Licensed under Apache 2.0                           *
* Available at http://www.apache.org/licenses/LICENSE-2.0.html                 *
\******************************************************************************/

using UnityEngine;
using System.Collections;

public class GrabbableObject : MonoBehaviour {
	/// <summary>
	/// The use axis alignment.
	/// </summary>
  public bool useAxisAlignment = true;
	/// <summary>
	/// The right hand axis.
	/// </summary>
  public Vector3 rightHandAxis;
	/// <summary>
	/// The object axis.
	/// </summary>
  public Vector3 objectAxis;
	/// <summary>
	/// The rotate quickly.
	/// </summary>
  public bool rotateQuickly = false;
	/// <summary>
	/// The center grabbed object.
	/// </summary>
  public bool centerGrabbedObject = false;
	/// <summary>
	/// The breakable joint.
	/// </summary>
  public Rigidbody breakableJoint;
	/// <summary>
	/// The break force.
	/// </summary>
  public float breakForce;
	/// <summary>
	/// The break torque.
	/// </summary>
  public float breakTorque;
	/// <summary>
	/// The grabbed_.
	/// </summary>
  protected bool grabbed_ = false;
	/// <summary>
	/// The hovered_.
	/// </summary>
  protected bool hovered_ = false;
	/// <summary>
	/// Determines whether this instance is hovered.
	/// </summary>
	/// <returns><c>true</c> if this instance is hovered; otherwise, <c>false</c>.</returns>
  public bool IsHovered() {
    return hovered_;
  }
	/// <summary>
	/// Determines whether this instance is grabbed.
	/// </summary>
	/// <returns><c>true</c> if this instance is grabbed; otherwise, <c>false</c>.</returns>
  public bool IsGrabbed() {
    return grabbed_;
  }
	/// <summary>
	/// Raises the start hover event and self iluminate the object.
	/// </summary>
  public virtual void OnStartHover() {
    hovered_ = true;
	//********************************************************
		this.GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Diffuse");		
	//********************************************************
  }
	/// <summary>
	/// Raises the stop hover event and puts the object self ilumination to normal.
	/// </summary>
  public virtual void OnStopHover() {
    hovered_ = false;
		//********************************************************
		this.GetComponent<Renderer> ().material.shader = Shader.Find("Diffuse");			
		//******************************************************
  }
	/// <summary>
	/// Raises the grab event.
	/// </summary>
  public virtual void OnGrab() {
    grabbed_ = true;
    hovered_ = false;

    if (breakableJoint != null) {
      Joint breakJoint = breakableJoint.GetComponent<Joint>();
      if (breakJoint != null) {
        breakJoint.breakForce = breakForce;
        breakJoint.breakTorque = breakTorque;
      }
    }
  }
	/// <summary>
	/// Raises the release event.
	/// </summary>
  public virtual void OnRelease() {
    grabbed_ = false;

    if (breakableJoint != null) {
      Joint breakJoint = breakableJoint.GetComponent<Joint>();
      if (breakJoint != null) {
        breakJoint.breakForce = Mathf.Infinity;
        breakJoint.breakTorque = Mathf.Infinity;
      }
    }
  }


}
//cube.renderer.material.shader = Shader.Find("Self-Illumin/Diffuse");